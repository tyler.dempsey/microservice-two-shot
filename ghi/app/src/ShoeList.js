async function deleteShoe(id){
  const shoeUrl= `http://localhost:8080/api/shoes/${id}`
  const fetchConfig = {
    method: "delete",
    headers: {
      'Content-Type': 'application/json',
    }
  }
  const response = await fetch(shoeUrl, fetchConfig);
  console.log(response);
  if (response.ok){
    window.location.reload(true);
  }


  }


function ShoeList ({shoes}){
    console.log("helllo", shoes)
    return (
      <div className="shoe-list">
          <h1 className="title" align="center">Shoes</h1>
          <table className="table align-middle">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes?.map(shoe => {
              return (
              <tr key = {shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td><img src={shoe.picture_url} alt="dog"/></td>
                <td>{shoe.bin}</td>
                <td><button type='button' className="btn btn-danger" onClick={() => deleteShoe(`${shoe.id}`)}>Delete</button></td>
              </tr>
              )
            })}
          </tbody>
        </table>
      </div>

    )
    }

export default ShoeList;
