import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatList';
import HatForm from './HatForm';
import React from 'react';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
import './index.css';

// class App extends React.Component {
//   constructor(props) {

//     super(props)
//     this.state={
//       hats: [],
//       shoes: [],
//     };
//   }

//   render() { 
function App(props){
  if (props.shoes === undefined && props.hats === undefined) {
    return null;
  }
  // const {shoes} = props;


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes = {props.shoes}/>} />
            <Route path ="new" element= {<ShoeForm/>} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    );
}

export default App;