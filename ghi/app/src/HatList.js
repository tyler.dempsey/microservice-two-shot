import React from "react"


class HatList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hats: []
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({hats: data.hats})
            console.log("state", this.state)
        }
    }

    async deleteHat(hat) {
        const hatUrl = `http://localhost:8090/api/hats/${hat.id}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const remHats = this.state.hats.filter((i) => hat.id != i.id)
            this.setState({hats: remHats})
        }
    }


    render() {
        return (
            <div className="container">
                <h1>Hat List</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Fabric</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Closet</th>
                            <th>Section</th>
                            <th>Shelf</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.hats.map(hat => {
                            return (
                                <tr key={hat.id}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style}</td>
                                    <td>{hat.color}</td>
                                    <td>
                                        <img style={{ width:50 }} src={hat.picture} alt=""></img>
                                    </td>
                                    <td>{hat.location.closet_name}</td>
                                    <td>{hat.location.section_number}</td>
                                    <td>{hat.location.shelf_number}</td>
                                    <td>
                                        <button type="button" className="btn btn-danger" onClick={() => this.deleteHat(hat)}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default HatList
