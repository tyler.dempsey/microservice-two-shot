import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// async function loadShoes() {
//   const response = await fetch('http://localhost:8080/api/shoes/')
//   if (response.ok){
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App shoes = {data.shoes} />
//       </React.StrictMode>
//     )
//     console.log("hi", data);
//   } else {
//     console.error(response)
//   }
// }

// loadShoes()

async function loadHatsShoes() {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');

  if (shoesResponse.ok && hatsResponse.ok)  {
    const shoeData = await shoesResponse.json();
    const hatData = await hatsResponse.json();
    console.log(shoeData)
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} hats={hatData}/>
      </React.StrictMode>
    );            
  } else {
    console.error(shoesResponse || hatsResponse);
  }
}
loadHatsShoes()
