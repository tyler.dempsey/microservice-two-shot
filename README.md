# Wardrobify

Team:

* Tyler Dempsey- hats
* Lesley Tomosada - shoes

## Design
![Diagram](/ghi/app/public/diagram.png)


## Hats microservice

1 - hats/api is not in django so I put the app into INSTALLED APPs.
2 - Where are no urls, views or models
3 - Added path('api/', include('hats_rest.urls)) to the urlpatterns
4 - Created git branch named tyler-branch
5 - Created Hats model that contains
        - fabric
        - ForeignKey to Locations (within the wardrobe)
        - stylename
        - url for picture using a models.URLField
        - color
6 - Create RESTful Api to get
        - List
        - Create a new hat
        - Delete a new Hat
7 - Create REACT components to
        - Show list of hats and their details
        - Show a form to create a new hat
8 - Provide a way to delete a hat
9 - Provide a existing nav link to your components

## Shoes microservice

The shoe model was created with the attributes of manufacturer, model name, color, picture and bin. A poller was created to fetch new Bin data from the Wardrobe API. Three views were created make RESTful API endpoints for GET, POST, and DELETE. These three endpoints were used to create two React components, one to view a list of shoes, and the other a form to create a new shoe. These components can be access via the nav bar on the homepage at localhost:3000.

In order to create a new shoe using the homepage, a bin must first be created. While this was outside of the scope of this project, a bin can be made via the admin panel. Once a bin exists, the form can be used to create a new shoe. This shoe can then be viewed on the list page, and subsequently be deleted if desired. 