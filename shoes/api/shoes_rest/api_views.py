from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import Shoe, BinVO
from common.json import ModelEncoder
import json

# Create your views here.
class ShoeListEncoder(ModelEncoder):
    model= Shoe
    properties = [
        "manufacturer", 
        "model_name", 
        "color", 
        "picture_url", 
        "bin",
        "id",
        # "href",
        ]

    def get_extra_data(self, o):
       return {"bin":o.bin.closet_name}

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number","bin_size","import_href",]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer", 
        "model_name", 
        "color", 
        "picture_url", 
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else: 
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Id"}, status = 400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

@require_http_methods(["DELETE"])
def api_detail_shoe(request,pk):
    if request.method == "DELETE":
        count,_ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count>0})