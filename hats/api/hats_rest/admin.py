from django.contrib import admin

from .models import LocationVO, Hat

@admin.register(LocationVO)
class PresentationAdmin(admin.ModelAdmin):
    pass


@admin.register(Hat)
class StatusAdmin(admin.ModelAdmin):
    pass
